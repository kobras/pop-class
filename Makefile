VENV = venv
PYTHON = $(VENV)/bin/python

PIP = $(VENV)/bin/pip
PIP_MIRROR = https://pypi.mirrors.ustc.edu.cn/simple

# `w` stands for `wipe`
PIP_INSTALL = $(PIP) install --quiet --exists-action=w

export NODE_ENV ?= dev
# dependency setup
setup: venv deps

deps:
	@$(PIP_INSTALL) -i $(PIP_MIRROR) -r requirements.txt

venv:
	@virtualenv $(VENV) --prompt '<venv:pop>'
	@$(PIP_INSTALL) -i $(PIP_MIRROR) -U pip setuptools

# db schema
db:
	$(PYTHON) manage.py migratedb
	@echo
	$(PYTHON) manage.py syncdb

clean_pyc:
	@find . -not \( -path './venv' -prune \) -name '*.pyc' -exec rm -f {} \;

server: clean_pyc
	$(PYTHON) manage.py runserver

npm:
	@npm install

wp-watch:
	@npm run dev

build:
	@npm run build

.PHONY: static
