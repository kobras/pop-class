var path = require('path');
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');

module.exports = {
  entry: [
    './front-src/index.js',
    'webpack-dev-server/client?http://localhost:8080',
  ],
  output: {
    path: path.resolve('./front-src/bundles/'),
    filename: '[name]-[hash].js',
    publicPath: 'http://localhost:8080/front-src/bundles/', // Tell django to use this URL to load packages and not use STATIC_URL + bundle_name
  },
  plugins: [
    new BundleTracker({filename: './webpack-stats.json'}),
  ],
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue',
      },
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'url',
        query: {
          limit: 500000,
          name: '[name].[ext]?[hash]',
        },
      },
    ],
  },
};
