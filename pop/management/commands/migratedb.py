# coding: utf-8

from optparse import make_option
from os import path
from subprocess import call, STDOUT

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from pop.libs.utils import get_app_root


class MySQLCmdError(CommandError):
    pass


class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option(
            '--test-mode',
            action='store_true',
            dest='test_mode',
            default=False,
            help='running in test mode'
        ),
        make_option(
            '--db-name',
            action='store',
            dest='db_name',
        ),
    )

    help = '**DEV ONLY**: drop and recreate all self-managed tables'

    MYSQL_CMD = 'mysql -h{HOST} -u{USER} {NAME}'
    TARGET = 'db/schema.sql'

    def handle(self, *args, **options):
        self.test_mode = options['test_mode']
        self.db_name = options.get('db_name')

        if not (settings.DEBUG or self.test_mode):
            raise CommandError('NOT ALLOWED IN PRODUCTION MODE.')

        for f in self.find_sql_files():
            self.stdout.write('Loading %s into MySQL' % f)
            self.load_sql(f)

    def load_sql(self, path):
        self.call_mysql('source %s' % path)

    def call_mysql(self, statement):
        """
        execute SQL statement by invoking MySQL client
        raise MySQLCmdError on error
        """

        db_config = settings.DATABASES['default'].copy()
        if self.db_name:
            db_config['NAME'] = self.db_name

        cmd = self.MYSQL_CMD.format(**db_config)
        cmd = cmd.split(' ')
        cmd.append('-e %s' % statement)

        if db_config.get('PASSWORD'):
            cmd.append('-p%s' % db_config['PASSWORD'])

        if call(cmd, stderr=STDOUT) != 0:
            raise MySQLCmdError('error on executing %s' % cmd)

    def find_sql_files(self):
        for app in settings.INSTALLED_APPS:
            sql_path = path.join(get_app_root(app), self.TARGET)
            if path.exists(sql_path):
                yield sql_path
