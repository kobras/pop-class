# coding: utf-8

from annoying.decorators import render_to
from django.template.response import TemplateResponse
from django.http import HttpResponseBadRequest, HttpResponse
from django.contrib.auth.models import AnonymousUser
from django.views.decorators.clickjacking import xframe_options_exempt

from homework.models.account import set_user_cookie, Account, del_user_cookie


@render_to()
def index(request):
    uuid = request.GET.get('user_id')
    password = request.GET.get('password')
    username = request.GET.get('username')
    kind = request.GET.get('kind')
    response = TemplateResponse(request, 'index.html')
    if isinstance(request.user, AnonymousUser):
        if not all([uuid, password, username, kind]):
            return HttpResponseBadRequest("参数错误")

        user = Account.get_or_create(uuid, password, username, kind)
        set_user_cookie(response, user)
    return response


@xframe_options_exempt
def logout(request):
    response = HttpResponse(request, "已经退出")
    del_user_cookie(response)
    return response
