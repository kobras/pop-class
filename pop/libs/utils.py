# coding: utf-8

from contextlib import contextmanager
from hashlib import md5
from importlib import import_module
from logging import Filter
from os import chdir as cd, getcwd, path, walk
from random import random
from time import time

from django.conf import settings
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError


def get_app_root(app):
    module = import_module(app)
    return path.dirname(module.__file__)


def find_all_py_files(basedir):
    for _path, _, files in walk(basedir):
        for f in files:
            if f.endswith('.py'):
                yield path.join(_path, f)


def find_all_submodules(module_dir, pkg_basename):
    prefix_len = len(module_dir)

    if not module_dir.endswith('/'):
        prefix_len += 1

    for py in find_all_py_files(module_dir):
        # e.g.: '/path/to/appname/pkg_basename/submod/sth.py' -> 'submod.sth'
        modname = path.splitext(py[prefix_len:])[0].replace('/', '.')

        # ignore pkg_basename.__init__ (the module that calls this function)
        if modname == '__init__':
            continue

        mod = import_module('.%s' % modname.replace('.__init__', ''),
                            package=pkg_basename)
        yield mod


def hash_filename(filename):
    name, ext = path.splitext(filename)
    name = md5('%s-%s-%s' %
               (time(), name.encode('utf-8'), random())).hexdigest()
    return name + ext


class BooleanLogFilter(Filter):

    def __init__(self, settings_key):
        self.key = settings_key

    def filter(self, _):
        return getattr(settings, self.key)


validator = URLValidator()


def validate_url(url):
    try:
        validator(url)
        return True
    except ValidationError:
        return False


@contextmanager
def chdir(new_dir):
    old_dir = getcwd()
    try:
        cd(new_dir)
        yield
    finally:
        cd(old_dir)
