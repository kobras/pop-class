# coding: utf-8

from django.conf import settings
from django.core.signing import SignatureExpired, BadSignature

from homework.models.account import (Account, set_user_cookie)


class UserCookieMiddleware(object):

    '''
    解析/设置用户cookie
    '''

    def process_request(self, request):
        # 解析cookie
        if request.COOKIES.get(settings.USER_COOKIE_KEY):
            try:
                user_id = request.get_signed_cookie(
                    settings.USER_COOKIE_KEY,
                    salt=settings.USER_COOKIE_SALT,
                    max_age=settings.USER_COOKIE_MAX_AGE
                )
                user_id = int(user_id)
                request.user = Account.get(user_id)
                # disable csrf
                setattr(request, '_dont_enforce_csrf_checks', True)
            except (BadSignature, SignatureExpired, ValueError):
                # 删除无效cookie，便于process_response()设置新cookie
                del request.COOKIES[settings.USER_COOKIE_KEY]

        return None

    def process_response(self, request, response):
        # 设置用户cookie
        user = hasattr(request, 'user') and request.user or None
        if request.COOKIES.get(settings.USER_COOKIE_KEY) is None\
                and isinstance(user, Account)\
                and response.status_code != 500:
            set_user_cookie(response, request.user)
        return response
