# coding: utf-8

from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from homework import urls as homework_api_urls
from pop.views import index, logout
from pop import settings


urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_DIST)

urlpatterns += patterns('',
    url(r'^f', index, name="index"),
    url(r'^logout$', logout, name="logout"),
    url(r'^api/', include(homework_api_urls)),
)
