# coding: utf-8

import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


SECRET_KEY = '*g#tiv9oc_&39fi1^^mez0xw^rvd6gaw&ej_1$tw-t1@a&zbc*'


WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'bundles/',
        'STATS_FILE': 'webpack-stats.json',
        'POLL_INTERVAL': 0.1,
        'IGNORE': ['.+\.hot-update.js', '.+\.map']
    }
}

DEBUG = True


TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']


INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'pop',
    'homework',
    'rest_framework',
    'webpack_loader',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'pop.middlewares.UserCookieMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

REST_FRAMEWORK = {
    "UNICODE_JSON": False
}

ROOT_URLCONF = 'pop.urls'

WSGI_APPLICATION = 'pop.wsgi.application'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'pop',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '127.0.0.1',
    }
}


TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True
USE_L10N = True
USE_TZ = False


STATIC_URL = '/static/'
STATIC_DIST = os.path.join(BASE_DIR, 'front-src')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'front-src'),
)

USER_COOKIE_KEY = 'uck'
USER_COOKIE_SALT = 'usercookie'
USER_COOKIE_MAX_AGE = 3600 * 24 * 30
LOGGING = {
    'version': 1,  # must be 1 according to Django doc
    'disable_existing_loggers': False,

    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },

    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'stdout': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },

    'loggers': {
        'django.security': {
            'handlers': ['null',],
            'level': 'INFO',
        },

        'django.request': {
            'handlers': ['stdout',],
            'level': 'INFO',
        },

        'pop': {
            'handlers': ['null',],
            'level': 'INFO',
            'propagate': False,
        },
    },
}
