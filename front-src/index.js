const AppView = require('./App.vue');
const KlassView = require('./views/Klass.vue');
const TeacherAssignmentDetailView = require('./views/teacher/AssignmentDetail.vue');
const TeacherKlassOverview = require('./views/teacher/KlassOverview.vue');
const TeacherAssignmentListView = require('./views/teacher/AssignmentList.vue');
const TeacherCreateAssignmentView = require('./views/teacher/CreateAssignment.vue');
const TeacherQuestionDetail = require('./views/teacher/QuestionDetail.vue');
const TeacherAssignmentPreview = require('./views/teacher/AssignmentPreview.vue');
const QuestionDetail = require('./views/QuestionDetail.vue');
const StudentAssignmentView = require('./views/student/Assignment.vue');
const StudentDoingErrorHomeworkView = require('./views/student/DoingErrorHomework.vue');
const Vue = require('vue');
const Router = require('vue-router');

Vue.use(Router);
Vue.use(require('vue-resource'));

const router = new Router({
  hashbang: false,
  history: true,
  saveScrollPosition: true,
});


router.map({
  '/f': {
    name: 'homepage',
    component: KlassView,
  },
  '/f/student/klass/:klass_id': {
    name: 'student_assignment',
    component: StudentAssignmentView,
  },
  '/f/teacher/klass/:klass_id': {
    name: 'teacher_klass_overview',
    component: TeacherKlassOverview,
  },
  '/f/teacher/klass/:klass_id/assignment/:assignment_id': {
    name: 'teacher_preview_assignment',
    component: TeacherAssignmentPreview,
  },
  '/f/teacher/klass/:klass_id/assignment': {
    name: 'teacher_assignment_list',
    component: TeacherAssignmentListView,
  },
  '/f/teacher/klass/:klass_id/create_assignment': {
    name: 'teacher_create_assignment',
    component: TeacherCreateAssignmentView,
  },
  '/f/teacher/assignment/:assignment_id/questions': {
    name: 'xxxxteacher_preview_assignment',
    component: TeacherAssignmentDetailView,
  },
  '/f/homework/:homework_id/klass/:klass_id/question/:stem_id': {
    name: 'question_detail',
    component: QuestionDetail,
  },
  '/f/assignment/:assignment_id/klass/:klass_id/question/:stem_id': {
    name: 'teacher_question_detail',
    component: TeacherQuestionDetail,
  },
  '/f/student/assignment/:assignment_id/:klass_id/error_questions/': {
    name: 'student_assignment_erro_questions',
    component: StudentDoingErrorHomeworkView,
  },
});

// router.redirect({
//   '*': '/index',
// });

router.beforeEach(function() {
  window.scrollTo(0, 0);
});

router.start(AppView, 'app');
