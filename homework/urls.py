# coding: utf-8

from django.conf.urls import patterns, url

from homework.api.klass import (
    klass_list, klass_detail, join_klass,
    kick_student_from_klass, update_book
)
from homework.api.question import question_tree, question_detail, pop_question_data
from homework.api.user import user_profile
from homework.api.book import get_books
from homework.api.assignment import (
    assignment_list, get_student_assignment_node_ids,
    get_student_assignments, submit_question,
    get_teacher_assignment_node_ids
)


urlpatterns = patterns(
    '',
    url(r'^klass/(?P<klass_id>\d+)$', klass_detail),
    url(r'^klass$', klass_list),
    url(r'^klass/(?P<klass_id>\d+)/assignment$', assignment_list),
    url(r'^book$', get_books),
    url(r'^klass/(?P<klass_id>\d+)/book/(?P<book_id>\d+)$', update_book),
    url(r'^join_klass', join_klass),
    url(r'^kick_student_from_klass/(?P<klass_id>\d+)/(?P<student_id>\d+)$',
        kick_student_from_klass),
    url(r'^user_profile$', user_profile),
    url(r'^question_tree/(?P<klass_id>\d+)$', question_tree),
    url(r'^question/(?P<node_id>\d+)$', question_detail),
    url(r'^pop_question/(?P<stem_id>\d+)$', pop_question_data),
    url(r'^homework/(?P<assignment_id>\d+)/question/(?P<stem_id>\d+)$', submit_question),
    url(r'^assignment/(?P<student_assignment_id>\d+)/node_ids$', get_student_assignment_node_ids),
    url(r'^teacher/assignment/(?P<assignment_id>\d+)/node_ids$', get_teacher_assignment_node_ids),
    url(r'^student/assignments/klass/(?P<klass_id>\d+)$', get_student_assignments),
)
