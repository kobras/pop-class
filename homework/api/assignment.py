# coding: utf-8

from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.core.exceptions import PermissionDenied
from django.http import Http404

from homework.api.utils import login_required
from homework.models.assignment import Assignment, StudentAssignment, StudentAssignmentStem
from homework.models.klass import Klass
from homework.serializers.assignment import (
    AssignmentSerializer, QuestionSerializer,
    StudentHistoryAssignmentSerializer
)


@login_required
@api_view(['GET', 'POST'])
def assignment_list(request, klass_id):
    teacher = request.user.teacher_profile
    if not teacher:
        raise PermissionDenied
    klass = Klass.get(klass_id)
    if not klass:
        raise Http404

    if request.method == "POST":
        book_id = klass.book_id
        teacher_id = teacher.id
        finish_time = request.data["finish_time"]
        questions = request.data["questions"]
        assignment = Assignment.add(klass_id, book_id, teacher_id, finish_time, questions)
        teacher.assign_homework(klass, assignment)
        return Response([])

    elif request.method == "GET":
        assignments = Assignment.get_multi_by_klass_id(klass_id)
        data = AssignmentSerializer(assignments, many=True).data
        return Response(data)


@login_required
@api_view(['GET'])
def get_student_assignment_node_ids(request, student_assignment_id):
    _type = request.GET.get("type")
    student_assi = StudentAssignment.get(student_assignment_id)
    if not student_assi:
        raise PermissionDenied
    if _type == "error":
        questions = student_assi.get_error_questions()
    else:
        questions = student_assi.assignment.get_questions()
    data = QuestionSerializer(questions, many=True).data
    return Response(data)


@login_required
@api_view(['GET'])
def get_teacher_assignment_node_ids(request, assignment_id):
    assi = Assignment.get(assignment_id)
    if not assi:
        raise PermissionDenied
    questions = assi.get_questions()
    data = QuestionSerializer(questions, many=True).data
    return Response(data)


@login_required
@api_view(['GET'])
def get_student_assignments(request, klass_id):
    student = request.user.student_profile
    s_assignments = StudentAssignment.get_multi_by_student_and_klass(student.id, klass_id)
    data = StudentHistoryAssignmentSerializer(s_assignments, many=True).data
    return Response(data)


@login_required
@api_view(['POST'])
def submit_question(request, assignment_id, stem_id):
    student = request.user.student_profile
    student_assi = StudentAssignment.get(assignment_id)
    if student and student_assi:
        instance = StudentAssignmentStem(stem_id, student_assi)
        instance.submit()
        return Response({'status': instance.status})
    else:
        raise PermissionDenied
