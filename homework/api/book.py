# coding: utf-8

from rest_framework.response import Response
from rest_framework.decorators import api_view

from homework.api.utils import login_required
from homework.models.book import Book
from homework.serializers.book import BookSerializer


@login_required
@api_view(['GET'])
def get_books(request):
    books = Book.get_all()
    data = BookSerializer(books, many=True).data
    return Response(data)
