# coding: utf-8

from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseNotFound, HttpResponseBadRequest

from homework.api.utils import login_required
from homework.models.klass import Klass, Student
from homework.serializers.klass import KlassSerializer
from homework.serializers.student import StudentSerializer


@login_required
@api_view(['GET', 'POST'])
def klass_list(request):
    if request.method == "GET":
        if request.user.teacher_profile:
            klasses = Klass.get_multi_by_teacher(request.user.teacher_profile.id)
            return Response(KlassSerializer(klasses, many=True).data)
        elif request.user.student_profile:
            klasses = request.user.student_profile.get_joined_klasses()
            return Response(KlassSerializer(klasses, many=True).data)

    elif request.method == "POST":
        name = request.data.get('name')
        book_id = request.data.get('book_id')
        teacher_id = request.user.teacher_profile.id
        Klass.add(name, teacher_id, book_id)
        klasses = Klass.get_multi_by_teacher(request.user.teacher_profile.id)
        return Response(KlassSerializer(klasses, many=True).data)


@login_required
@api_view(['GET'])
def klass_detail(request, klass_id):
    klass = Klass.get(klass_id)
    students = klass.get_students()
    data = {
        "students": StudentSerializer(students, many=True).data,
        "klass": KlassSerializer(klass).data
    }
    return Response(data)


@login_required
@api_view(['get'])
def join_klass(request):
    code = request.GET.get('code')
    klass = Klass.get_by_code(code)
    student = request.user.student_profile
    if not student:
        raise PermissionDenied
    if not klass:
        return HttpResponseNotFound('班级码不存在，请确认输入是否正确')
    if klass.has_joined(student.id):
        return HttpResponseBadRequest('已经加入这个班级')
    student.join(klass.id)
    return Response(KlassSerializer(klass).data)


@login_required
@api_view(['POST'])
def kick_student_from_klass(request, klass_id, student_id):
    teacher = request.user.teacher_profile
    if not teacher:
        raise PermissionDenied
    student = Student.get(student_id)
    if student:
        student.left(klass_id)
    return Response([])


@login_required
@api_view(['POST'])
def update_book(request, klass_id, book_id):
    klass = Klass.get(klass_id)
    klass.update_book(book_id)
    return Response({})
