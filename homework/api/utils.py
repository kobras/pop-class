# coding: utf-8

from django.http.request import HttpRequest
from django.core.exceptions import PermissionDenied

from homework.models.account import Account


def login_required(func):
    def wrapper(*args, **kwargs):
        if isinstance(args[0], HttpRequest):
            request = args[0]
        else:
            request = args[1]
        user = getattr(request, 'user', None)
        if not isinstance(user, Account):
            raise PermissionDenied
        return func(*args, **kwargs)

    return wrapper
