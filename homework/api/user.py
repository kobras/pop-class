# coding: utf-8

from rest_framework.response import Response
from rest_framework.decorators import api_view

from homework.api.utils import login_required


@login_required
@api_view(['GET'])
def user_profile(request):
    user = request.user
    if user.teacher_profile:
        data = {
            "username": user.teacher_profile.username,
            "is_student": False
        }
    elif user.student_profile:
        data = {
            "username": user.student_profile.username,
            "is_student": True
        }
    return Response(data)
