# coding: utf-8

from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.http import Http404

from homework.data.question_tree import parse_data as get_question_tree
from homework.data.question_detail import fetch_data as get_question_detail
from homework.api.utils import login_required
from homework.models.klass import Klass
from homework.models.assignment import AssignmentStem
from homework.serializers.assignment import QuestionSerializer


@login_required
@api_view(['GET'])
def question_tree(request, klass_id):
    if not klass_id:
        raise Http404
    klass = Klass.get(klass_id)
    if not klass:
        raise Http404
    app_id = klass.book.app_id
    user_id = request.user.uuid
    password = request.user.password
    data = get_question_tree(user_id, password, app_id)
    return Response(data)


@login_required
@api_view(['GET'])
def question_detail(request, node_id):
    unit = request.GET["unit"]
    user_id = request.user.uuid
    password = request.user.password
    data = get_question_detail(user_id, password, unit, node_id)
    return Response(data)


@login_required
@api_view(['GET'])
def pop_question_data(request, stem_id):
    instance = AssignmentStem.get(stem_id)
    return Response(QuestionSerializer(instance).data)
