# coding: utf-8

from django.core.files import File
from django.db import models
from annoying.functions import get_object_or_None

from homework.models.utils import upload_file_path_generator


def cover_path_generator(profile, filename):
    return upload_file_path_generator(filename, 'cover')


class Book(models.Model):

    class Meta(object):
        db_table = "homework_book"
        verbose_name = verbose_name_plural = u"教材"

    name = models.CharField(max_length=50, default='')
    app_id = models.PositiveIntegerField()
    cover = models.ImageField(
        upload_to=cover_path_generator,
        null=True
    )

    @classmethod
    def get(cls, pk):
        return get_object_or_None(cls, pk=pk)

    @classmethod
    def add(cls, name, app_id, path):
        instance = cls.objects.create(name=name, app_id=app_id)
        file = open(path)
        instance.cover.save(name, File(file))
        return instance

    @classmethod
    def get_all(cls):
        return cls.objects.all()

    @property
    def cover_url(self):
        if self.cover:
            return self.cover.url
        else:
            return
