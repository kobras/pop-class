# coding: utf-8

from __future__ import absolute_import

from django.db import models
from annoying.functions import get_object_or_None

from homework.models.utils import generate_klass_code


class Teacher(models.Model):

    class Meta(object):
        db_table = "homework_teacher"
        verbose_name = verbose_name_plural = u"老师"

    account_id = models.PositiveIntegerField(unique=True)
    username = models.CharField(max_length=50, default='')

    @classmethod
    def get(cls, pk):
        return get_object_or_None(cls, pk=pk)

    def get_klasses(self):
        return Klass.get_multi_by_teacher(self.pk)

    @classmethod
    def add(cls, account_id, username):
        return cls.objects.create(account_id=account_id, username=username)

    def add_klass(self, name):
        return Klass.add(name, self.id)

    @classmethod
    def get_by_account_id(cls, account_id):
        return get_object_or_None(cls, account_id=account_id)

    def assign_homework(self, klass, assignment):
        students = klass.get_students()
        for student in students:
            student.assign_homework(klass, assignment)


class Klass(models.Model):

    name = models.CharField(max_length=25)
    teacher_id = models.PositiveIntegerField()
    book_id = models.PositiveIntegerField()
    code = models.CharField(max_length=6, unique=True)

    class Meta(object):
        db_table = "homework_klass"
        verbose_name = verbose_name_plural = u"班级"

    @classmethod
    def get(cls, pk):
        return get_object_or_None(cls, pk=pk)

    @classmethod
    def get_by_code(cls, code):
        return get_object_or_None(cls, code=code)

    def get_students(self):
        student_ids = KlassStudent.get_student_ids_by_klass_id(self.id)
        return [Student.get(student_id) for student_id in student_ids]

    @property
    def book(self):
        from homework.models.book import Book
        return Book.get(self.book_id)

    @classmethod
    def get_multi_by_teacher(cls, teacher_id):
        return cls.objects.filter(teacher_id=teacher_id)

    @classmethod
    def add(cls, name, teacher_id, book_id):
        code = generate_klass_code(6)
        while get_object_or_None(cls, code=code):
            code = generate_klass_code(6)
        return cls.objects.create(name=name, teacher_id=teacher_id, book_id=book_id, code=code)

    @property
    def student_count(self):
        return len(KlassStudent.get_student_ids_by_klass_id(self.id, limit=0))

    def owned_by(self, user_id):
        return self.teacher_id == int(user_id)

    @property
    def teacher(self):
        return Teacher.get(self.teacher_id)

    def has_joined(self, student_id):
        return bool(KlassStudent.get(self.pk, student_id))

    def update_book(self, book_id):
        self.book_id = book_id
        self.save(update_fields=['book_id'])


class KlassStudent(models.Model):

    class Meta(object):
        db_table = "homework_klass_student"
        unique_together = ("klass_id", "student_id")
        index_together = (("student_id", "klass_id"),)
        verbose_name = verbose_name_plural = u"班级-学生"

    klass_id = models.PositiveIntegerField()
    student_id = models.PositiveIntegerField()

    @classmethod
    def get(cls, klass_id, student_id):
        return get_object_or_None(cls, klass_id=klass_id, student_id=student_id)

    @classmethod
    def get_student_ids_by_klass_id(cls, klass_id):
        return cls.objects.filter(klass_id=klass_id).values_list("student_id", flat=True)

    @classmethod
    def add(cls, klass_id, student_id):
        return cls.objects.get_or_create(klass_id=klass_id, student_id=student_id)[0]

    def remove(self):
        self.delete()

    @classmethod
    def get_klass_ids_student_id(cls, student_id):
        return cls.objects.filter(student_id=student_id).values_list("klass_id", flat=True)


class Student(models.Model):

    class Meta(object):
        db_table = "homework_student"
        verbose_name = verbose_name_plural = u"学生"

    account_id = models.PositiveIntegerField(unique=True)
    username = models.CharField(max_length=50, default='')

    @classmethod
    def get(cls, pk):
        return get_object_or_None(cls, pk=pk)

    @classmethod
    def get_by_account_id(cls, account_id):
        return get_object_or_None(cls, account_id=account_id)

    def get_klasses(self):
        klass_ids = KlassStudent.get_klass_ids_student_id(self.id)
        return [Klass.get(klass_id) for klass_id in klass_ids]

    @classmethod
    def add(cls, account_id, username):
        return cls.objects.create(account_id=account_id, username=username)

    def join(self, klass_id):
        KlassStudent.add(klass_id, self.pk)

    def left(self, klass_id):
        target = KlassStudent.get(klass_id, self.id)
        if target:
            target.remove()

    def assign_homework(self, klass, assignment):
        from homework.models.assignment import StudentAssignment
        StudentAssignment.get_or_create(self.id, assignment.id, klass.id)

    def get_joined_klasses(self):
        klass_ids = KlassStudent.get_klass_ids_student_id(self.id)
        return [Klass.get(klass_id) for klass_id in klass_ids]

    @property
    def account(self):
        from homework.models.account import Account
        return Account.get(self.account_id)
