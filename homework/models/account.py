# coding: utf-8

from __future__ import absolute_import
import time

from django.utils import timezone
from django.utils.http import cookie_date
from django.db import models
from django.conf import settings
from django.contrib.auth.hashers import is_password_usable
from annoying.functions import get_object_or_None

from homework.consts import USER_STATUS_OK, PROFILE_KIND_TEACHER, PROFILE_KIND_STUDENT

_MC_KEY_ACCOUNT = 'homework:teacher:%s'


class Account(models.Model):

    class Meta(object):
        db_table = "homework_account"
        app_label = "homework"

    uuid = models.CharField(max_length=32, unique=True)
    password = models.CharField(max_length=32)
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(default=timezone.now)
    status = models.PositiveSmallIntegerField(default=USER_STATUS_OK)

    def __unicode__(self):
        return "<Account %s>" % (self.pk)

    @classmethod
    def create(cls, uuid, password, status=USER_STATUS_OK):
        return cls.objects.create(uuid=uuid, password=password)

    @classmethod
    def add(cls, uuid, password, username, kind):
        from homework.models.klass import Teacher, Student
        account = cls.create(uuid, password)
        kind = int(kind)
        if kind == PROFILE_KIND_TEACHER:
            Teacher.add(account.id, username)
        elif kind == PROFILE_KIND_STUDENT:
            Student.add(account.id, username)
        return account

    @classmethod
    def get_or_create(cls, uuid, password, username, kind):
        from homework.models.klass import Teacher, Student
        account, created = cls.objects.get_or_create(uuid=uuid, defaults={"password": password})
        if created:
            if not account.check_password(password):
                account.set_password(password)
            kind = int(kind)
            if kind == PROFILE_KIND_TEACHER:
                Teacher.add(account.id, username)
            elif kind == PROFILE_KIND_STUDENT:
                Student.add(account.id, username)
        return account

    def set_password(self, raw_password):
        self.password = raw_password
        self.save(update_fields=["password"])

    def check_password(self, raw_password):
        return self.password == raw_password

    def has_usable_password(self):
        return is_password_usable(self.password)

    @property
    def is_active(self):
        return self.status == USER_STATUS_OK

    def is_authenticated(self):
        return True

    @classmethod
    def get(cls, pk):
        return get_object_or_None(cls, pk=pk)

    def set_status(self, status):
        self.status = status
        self.save(update_fields=['status'])

    def update_last_login(self):
        self.last_login = timezone.now()
        self.save(update_fields=["last_login"])

    @property
    def teacher_profile(self):
        from homework.models.klass import Teacher
        return Teacher.get_by_account_id(self.id)

    @property
    def student_profile(self):
        from homework.models.klass import Student
        return Student.get_by_account_id(self.id)

    @classmethod
    def get_by_uuid(cls, uuid):
        return get_object_or_None(cls, uuid=uuid)


def authenticate_by_uuid(uuid, password):
    user = Account.get_by_uuid(uuid)
    if user and user.check_password(password):
        return user
    else:
        return None


def set_user_cookie(response, user, remember=True):
    expires = None
    if remember:
        expires_time = time.time() + settings.USER_COOKIE_MAX_AGE
        expires = cookie_date(expires_time)
    response.set_signed_cookie(settings.USER_COOKIE_KEY,
                               str(user.id),
                               salt=settings.USER_COOKIE_SALT,
                               httponly=True,
                               max_age=settings.USER_COOKIE_MAX_AGE,
                               expires=expires)


def del_user_cookie(response):
    response.delete_cookie(settings.USER_COOKIE_KEY)
