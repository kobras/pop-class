# coding: utf-8

from __future__ import absolute_import

import os
import random
from hashlib import md5
from datetime import date
from time import time


_RANDOM_LETTERS = 'abcdefghijkmnpqrstuvwxyz23456789'


def generate_klass_code(length):
    return ''.join(random.choice(_RANDOM_LETTERS) for _ in range(length))


def upload_file_path_generator(filename, dir_name):
    '''生成上传文件保存的路径'''
    new_filename = hash_filename(filename)
    date_str = date.today().strftime('%Y/%m/%d')
    return os.path.join(dir_name, date_str, new_filename)


def hash_filename(filename):
    name, ext = os.path.splitext(filename)
    name = md5('%s-%s-%s' %
               (time(), name.encode('utf-8'), random.random())).hexdigest()
    return name + ext
