# coding: utf-8

from __future__ import absolute_import


from django.db import models
from annoying.functions import get_object_or_None

from homework.data.question_error import fetch_answer_status
from homework.consts import (
    ASSIGNMENT_STATUS_UNFINISHED,
    ASSIGNMENT_STATUS_FINISHED,
    ANSWER_NOT_GIVEN,
    ANSWER_RIGHT,
    ANSWER_WRONG
)


class Assignment(models.Model):

    class Meta(object):
        db_table = "homework_assignment"
        verbose_name = verbose_name_plural = u"作业"

    klass_id = models.PositiveIntegerField()
    book_id = models.PositiveIntegerField()
    teacher_id = models.PositiveIntegerField()
    publish_time = models.DateTimeField(auto_now_add=True)
    finish_time = models.DateTimeField(null=True)

    @property
    def name(self):
        return self.publish_time.strftime("%Y年%m月%d日")

    @property
    def formatted_publish_time(self):
        return self.publish_time.strftime("%Y年%m月%d日")

    @property
    def formatted_finish_time(self):
        return self.finish_time.strftime("%Y年%m月%d日")

    @classmethod
    def get(cls, pk):
        return get_object_or_None(cls, pk=pk)

    @property
    def klass(self):
        from homework.models.klass import Klass
        return Klass.get(self.klass_id)

    @classmethod
    def add(cls, klass_id, book_id, teacher_id, finish_time, questions):
        assignment = cls.objects.create(klass_id=klass_id, book_id=book_id,
                                        teacher_id=teacher_id, finish_time=finish_time)
        for kwargs in questions:
            AssignmentStem.add(assignment.id, **kwargs)
        return assignment

    @classmethod
    def get_multi_by_klass_id(cls, klass_id):
        return cls.objects.filter(klass_id=klass_id)

    def get_questions(self):
        return AssignmentStem.get_multi_by_assignment_id(self.id)


class AssignmentStem(models.Model):

    class Meta(object):
        db_table = "homework_assignment_stem"
        verbose_name = verbose_name_plural = u"作业的题目"

    assignment_id = models.PositiveIntegerField()
    node_id = models.PositiveIntegerField()
    unit = models.CharField(max_length=40, default="")
    lesson_name = models.CharField(max_length=40, default="")

    @classmethod
    def get(cls, pk):
        return get_object_or_None(cls, pk=pk)

    @classmethod
    def add(cls, assignment_id, node_id, unit, lesson_name):
        return cls.objects.create(assignment_id=assignment_id, node_id=node_id,
                                  unit=unit, lesson_name=lesson_name)

    @classmethod
    def get_multi_by_assignment_id(cls, assignment_id):
        return cls.objects.filter(assignment_id=assignment_id)


class StudentAssignment(models.Model):

    class Meta(object):
        db_table = "homework_student_assignment"
        verbose_name = verbose_name_plural = u"学生和作业的关联表"
        unique_together = ("student_id", "assignment_id")

    student_id = models.PositiveIntegerField()
    assignment_id = models.PositiveIntegerField()
    klass_id = models.PositiveIntegerField()

    @property
    def status(self):
        questions = self.get_questions()
        if all([StudentAssignmentStem(question.id, self).is_submitted() for question in questions]):
            return ASSIGNMENT_STATUS_FINISHED
        else:
            return ASSIGNMENT_STATUS_UNFINISHED

    @classmethod
    def get(cls, pk):
        return get_object_or_None(cls, pk=pk)

    @classmethod
    def get_by_student_and_assignment(cls, student_id, assignment_id):
        return get_object_or_None(cls, student_id=student_id, assignment_id=assignment_id)

    @classmethod
    def get_or_create(cls, student_id, assignment_id, klass_id=None):
        result, _ = cls.objects.get_or_create(student_id=student_id, assignment_id=assignment_id,
                                              defaults={"klass_id": klass_id})
        return result

    @property
    def assignment(self):
        return Assignment.get(self.assignment_id)

    @property
    def has_finished(self):
        return self.status == ASSIGNMENT_STATUS_FINISHED

    @classmethod
    def get_multi_by_assignment_id(cls, assignment_id):
        return cls.objects.filter(assignment_id=assignment_id)

    @property
    def student(self):
        from homework.models.klass import Student
        return Student.get(self.student_id)

    @classmethod
    def get_multi_by_student_and_klass(cls, student_id, klass_id):
        return cls.objects.filter(student_id=student_id, klass_id=klass_id)

    def get_questions(self):
        return AssignmentStem.get_multi_by_assignment_id(self.assignment_id)

    def submit(self):
        from homework.models.question_record import QuestionRecord
        account = self.student.account
        user_id = account.uuid
        password = account.password
        for question in self.get_questions():
            status = fetch_answer_status(user_id, password, question.node_id)
            QuestionRecord.get_or_create(self.student_id, self.assignment_id,
                                         self.klass_id, question.id, status)

    def get_error_questions(self):
        from homework.models.question_record import QuestionRecord
        ids = QuestionRecord.get_error_questions_ids(self.student_id, self.assignment_id,
                                                     self.klass_id)
        return [AssignmentStem.get(pk) for pk in ids]


class StudentAssignmentStem(object):

    def __init__(self, stem_id, student_assignment):
        self.stem_id = stem_id
        self.student_assignment = student_assignment
        self.account = student_assignment.student.account
        self.klass_id = student_assignment.klass_id
        self.assignment_id = student_assignment.assignment_id
        self.student_id = student_assignment.student_id

    @property
    def stem(self):
        return AssignmentStem.get(self.stem_id)

    @property
    def status(self):
        from homework.models.question_record import QuestionRecord
        student_id = self.student_assignment.student_id
        assignment_id = self.student_assignment.assignment_id
        klass_id = self.student_assignment.klass_id
        record = QuestionRecord.get_by_student_and_assignment_and_stem(
            student_id, assignment_id, klass_id, self.stem_id)
        if not record:
            return ANSWER_NOT_GIVEN
        else:
            return record.status

    def is_submitted(self):
        return self.status in (ANSWER_RIGHT, ANSWER_WRONG)

    def submit(self):
        from homework.models.question_record import QuestionRecord
        user_id = self.account.uuid
        password = self.account.password
        status = fetch_answer_status(user_id, password, self.stem.node_id)
        QuestionRecord.get_or_create(self.student_id, self.assignment_id,
                                     self.klass_id, self.stem.id, status)
