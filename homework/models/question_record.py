# coding: utf-8

from __future__ import absolute_import

from annoying.functions import get_object_or_None
from django.db import models
from homework.consts import ANSWER_WRONG, ANSWER_NULL


class QuestionRecord(models.Model):

    class Meta(object):
        db_table = "homework_question_record"
        app_label = "homework"
        verbose_name = verbose_name_plural = u"作业做题记录"

    student_id = models.PositiveIntegerField()
    assignment_id = models.PositiveIntegerField()
    klass_id = models.PositiveIntegerField()
    stem_id = models.PositiveIntegerField()
    status = models.PositiveSmallIntegerField(verbose_name="答题结果")

    @classmethod
    def get(cls, id_):
        return get_object_or_None(cls, pk=id_)

    @classmethod
    def get_or_create(cls, student_id, assignment_id, klass_id, stem_id, status):
        obj, created = cls.objects.get_or_create(student_id=student_id, assignment_id=assignment_id,
                                                 klass_id=klass_id, stem_id=stem_id,
                                                 defaults={"status": status})
        if not created:
            obj.update_status(status)
        return obj

    @classmethod
    def get_by_student_and_assignment_and_stem(cls, student_id, assignment_id, klass_id, stem_id):
        return get_object_or_None(cls, student_id=student_id, assignment_id=assignment_id,
                                  klass_id=klass_id, stem_id=stem_id)

    def update_status(self, status):
        self.status = status
        self.save(update_fields=["status"])

    @property
    def student(self):
        from homework.models.klass import Student
        return Student.get(self.student_id)

    @classmethod
    def get_error_questions_ids(cls, student_id, assignment_id, klass_id):
        return cls.objects.filter(
            student_id=student_id, assignment_id=assignment_id,
            klass_id=klass_id, status__in=(ANSWER_WRONG, ANSWER_NULL)).\
            values_list("stem_id", flat=True)
