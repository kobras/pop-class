# coding: utf-8

from rest_framework import serializers

from homework.serializers.base import DynamicFieldsSerializer
from homework.consts import (
    ASSIGNMENT_STATUS_UNFINISHED, ASSIGNMENT_STATUS_TEXT,
    ASSIGNMENT_STATUS_FINISHED, ANSWER_RIGHT
)
from homework.models.assignment import StudentAssignment, StudentAssignmentStem


class AssignmentSerializer(DynamicFieldsSerializer):

    id = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()
    formatted_publish_time = serializers.ReadOnlyField()
    formatted_finish_time = serializers.ReadOnlyField()
    unfinished_count = serializers.SerializerMethodField()
    total_count = serializers.SerializerMethodField()
    students_status = serializers.SerializerMethodField()

    def get_unfinished_count(self, instance):
        count = 0
        for item in self.get_students_status(instance):
            if item["status"] == ASSIGNMENT_STATUS_UNFINISHED:
                count += 1
        return count

    def get_total_count(self, instance):
        return len(self.get_students_status(instance))

    def get_students_status(self, instance):
        qs = StudentAssignment.get_multi_by_assignment_id(instance.id)
        return StudentAssignmentSerializer(qs, many=True).data


class StudentAssignmentSerializer(DynamicFieldsSerializer):
    id = serializers.ReadOnlyField()
    name = serializers.SerializerMethodField("get_student_username")
    status_text = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    def get_student_username(self, instance):
        return instance.student.username

    def get_status_text(self, instance):
        return ASSIGNMENT_STATUS_TEXT.get(instance.status)

    def get_status(self, instance):
        return instance.status


class QuestionSerializer(DynamicFieldsSerializer):
    id = serializers.ReadOnlyField()
    unit = serializers.ReadOnlyField()
    node_id = serializers.ReadOnlyField()
    lesson_name = serializers.ReadOnlyField()


class StudentHistoryAssignmentSerializer(DynamicFieldsSerializer):
    id = serializers.ReadOnlyField()
    name = serializers.SerializerMethodField("get_assignment_name")
    is_finished = serializers.SerializerMethodField()
    has_error = serializers.SerializerMethodField()
    questions = serializers.SerializerMethodField()

    def get_assignment_name(self, instance):
        return instance.assignment.name

    def get_is_finished(self, instance):
        return instance.status == ASSIGNMENT_STATUS_FINISHED

    def get_has_error(self, instance):
        return len(instance.get_error_questions()) > 0

    def get_questions(self, instance):
        qs = instance.get_questions()
        return StudentStemSerializer(qs, many=True, context={"student_assignment": instance}).data


class StudentStemSerializer(DynamicFieldsSerializer):

    id = serializers.ReadOnlyField()
    unit = serializers.ReadOnlyField()
    node_id = serializers.ReadOnlyField()
    status = serializers.SerializerMethodField()
    lesson_name = serializers.ReadOnlyField()
    is_finished = serializers.SerializerMethodField()

    def get_status(self, instance):
        student_assignment = self.context.get('student_assignment')
        return StudentAssignmentStem(instance.id, student_assignment).status

    def get_is_finished(self, instance):
        return self.get_status(instance) == ANSWER_RIGHT
