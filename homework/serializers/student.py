# coding: utf-8

from rest_framework import serializers

from homework.serializers.base import DynamicFieldsSerializer


class StudentSerializer(DynamicFieldsSerializer):

    id = serializers.ReadOnlyField()
    username = serializers.ReadOnlyField()
    account_id = serializers.ReadOnlyField()
