# coding: utf-8

from rest_framework import serializers

from homework.serializers.base import DynamicFieldsSerializer
from homework.models.book import Book


class KlassSerializer(DynamicFieldsSerializer):

    id = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()
    code = serializers.ReadOnlyField()
    book_id = serializers.ReadOnlyField()
    book_cover_url = serializers.SerializerMethodField()

    def get_book_cover_url(self, instance):
        return Book.get(instance.book_id).cover_url
