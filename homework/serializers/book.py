# coding: utf-8

from rest_framework import serializers

from homework.serializers.base import DynamicFieldsSerializer


class BookSerializer(DynamicFieldsSerializer):

    id = serializers.ReadOnlyField()
    name = serializers.ReadOnlyField()
    cover_url = serializers.ReadOnlyField()
