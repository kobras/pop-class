# coding: utf-8

import os

from django.core.management.base import BaseCommand

from homework.models.book import Book
from homework.models.account import Account
from homework.models.klass import Klass
from homework.consts import PROFILE_KIND_TEACHER, PROFILE_KIND_STUDENT
from pop.settings import MEDIA_ROOT

IMAGE_FOLDER = os.path.join(os.path.dirname(__file__), 'book-covers')


BOOKS_DATA = [
    {"name": u"基础1", "app_id": 1},
    {"name": u"基础2", "app_id": 2},
    {"name": u"基础3", "app_id": 3},
    {"name": u"基础4", "app_id": 25},
    {"name": u"基础5", "app_id": 44},
    {"name": u"基础6", "app_id": 45},
    {"name": u"进阶1", "app_id": 8},
    {"name": u"进阶2", "app_id": 9},
    {"name": u"进阶3", "app_id": 10},
    {"name": u"进阶4", "app_id": 46},
    {"name": u"进阶5", "app_id": 48},
    {"name": u"进阶6", "app_id": 51},
    {"name": u"飞跃1", "app_id": 24},
    {"name": u"飞跃2", "app_id": 54},
    {"name": u"青少英语一级（上）", "app_id": 26},
    {"name": u"青少英语一级（下）", "app_id": 27},
    {"name": u"青少英语二级（上）", "app_id": 28},
    {"name": u"青少英语二级（下）", "app_id": 29},
    {"name": u"青少英语三级（上）", "app_id": 30},
    {"name": u"青少英语三级（下）", "app_id": 31},
    {"name": u"青少英语四级（上）", "app_id": 32},
    {"name": u"青少英语四级（下）", "app_id": 43},
    {"name": u"青少英语五级（上）", "app_id": 34},
    {"name": u"青少英语五级（下）", "app_id": 35},
    {"name": u"青少英语六级（上）", "app_id": 36},
    {"name": u"青少英语六级（下）", "app_id": 37},
    {"name": u"青少英语七级（上）", "app_id": 38},
    {"name": u"青少英语七级（下）", "app_id": 42},
    {"name": u"青少英语八级（上）", "app_id": 40},
    {"name": u"青少英语八级（下）", "app_id": 41},
    {"name": u"精讲精练1册（1上）", "app_id": 57},
    {"name": u"精讲精练1册（1下）", "app_id": 58},
    {"name": u"精讲精练1册（2上）", "app_id": 59},
    {"name": u"精讲精练1册（2下）", "app_id": 60},
    {"name": u"精讲精练1册（3上）", "app_id": 61},
    {"name": u"精讲精练1册（3下）", "app_id": 62},
    {"name": u"精讲精练1册（4上）", "app_id": 63},
    {"name": u"精讲精练1册（4下）", "app_id": 64},
    {"name": u"精讲精练2册（1上）", "app_id": 65},
    {"name": u"精讲精练2册（1下）", "app_id": 66},
    {"name": u"精讲精练2册（2上）", "app_id": 67},
    {"name": u"精讲精练2册（2下）", "app_id": 68},
    {"name": u"精讲精练2册（3上）", "app_id": 69},
    {"name": u"精讲精练2册（3下）", "app_id": 70},
    {"name": u"精讲精练2册（4上）", "app_id": 71},
    {"name": u"精讲精练2册（4下）", "app_id": 72},
]


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.import_teachers()
        self.import_students()
        self.import_books()

    def import_books(self):
        for kwargs in BOOKS_DATA:
            kwargs["path"] = os.path.join(IMAGE_FOLDER, "%s.png" % kwargs["app_id"])
            Book.add(**kwargs)

    def import_teachers(self):
        uuid = "8a488fb44a50a3cf014a50a4b2c10000"
        password = "21367EE73FB48E561E024E47DEA149D6"
        username = "周老师"
        account = Account.add(uuid, password, username, PROFILE_KIND_TEACHER)
        book_id = 3
        Klass.add("新概念小班", account.teacher_profile.id, book_id)

    def import_students(self):
        uuid = "11188fb44a50a3cf014a50a4b2c10000"
        password = "21367EE73FB48E561E024E47DEA149D6"
        username = "李小花"
        Account.add(uuid, password, username, PROFILE_KIND_STUDENT)
