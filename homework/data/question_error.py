# coding: utf-8

import time

import requests

from homework.data.utils import get_md5
from homework.consts import ANSWER_NULL, ANSWER_RIGHT, ANSWER_WRONG


def fetch_data(user_id, password, node_id):
    url = "http://www.popnew.cn/actionService/gateway"
    _time = "%d" % time.time()
    web_app_id = 12
    params = {
        "serviceName": "SwfTestService",
        "methodName": "getTestRecord",
        "nodeId": node_id,
        "md5": get_md5(user_id, password, web_app_id, _time),
        "userId": user_id,
        "webAppId": web_app_id,
        "pwd": password,
        "randtime": _time,
        "time": _time,
    }
    r = requests.get(url, params=params)
    return r.json()


def fetch_answer_status(user_id, password, node_id):
    content = fetch_data(user_id, password, node_id)
    if not content:
        return ANSWER_NULL
    else:
        answer_detail = content[u"testRecord"]
        if not len(answer_detail):
            return ANSWER_NULL
        elif all([answer["isRight"] == 1 for answer in answer_detail]):
            return ANSWER_RIGHT
        else:
            return ANSWER_WRONG
