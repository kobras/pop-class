# coding: utf-8

import requests


def fetch_data(user_id, password, unit, node_id):
    # url = "http://kidsfile.popnew.cn/2015/06/01/1433120510749/popMobile/test.html"
    # url = "http://kidsfile.popnew.cn/2016/02/26/1456484235404/popMobile/test.html"
    url = "http://kidsfile.popnew.cn/2016/02/29/1456716291703/popMobile/test.html"
    web_app_id = 12
    params = {
        "nodeId": node_id,
        "path": "0_0_0",
        "title": unit,
        "webAppId": web_app_id,
        "userId": user_id,
        "appPwd": password,
        "timeOffset": "-8365",
        "serverIp": "http://popnew.cn:80/",
        "mode": "0",
    }
    r = requests.get(url, params=params)
    return r.request.url
