# coding: utf-8

from hashlib import md5


def get_md5(user_id, password, web_app_id, time):
    return md5("%s%s%s%s" % (web_app_id, password, user_id, time)).hexdigest()
