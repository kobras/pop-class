# coding: utf-8

import time

import requests

from homework.data.utils import get_md5


def _fetch_data(user_id, password, app_id):
    url = "http://popnew.cn/actionService/gateway"
    _time = "%d" % time.time()
    web_app_id = 12
    params = {
        "serviceName": "SwfSyncExecAppNode",
        "methodName": "getPCMobileNodeTree",
        "appId": app_id,
        "md5": get_md5(user_id, password, web_app_id, _time),
        "userId": user_id,
        "webAppId": web_app_id,
        "pwd": password,
        "time": _time
    }
    r = requests.get(url, params=params)
    return r.json()


def parse_data(user_id, password, app_id):
    data = _fetch_data(user_id, password, app_id)["nodeTreeJson"]
    for unit in data:
        [unit.pop(k) for k in unit.keys() if k not in ("node", "children")]
        for chapter in unit["children"]:
            [chapter.pop(k) for k in chapter.keys() if k not in ("node", "children")]
            for section in chapter["children"]:
                [section.pop(k) for k in section.keys() if k not in ("node", "children")]
                for lesson in section["children"]:
                    [lesson.pop(k) for k in lesson.keys() if k not in ("node", "id")]
    return data
