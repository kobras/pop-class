/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

DROP TABLE IF EXISTS `homework_account`;
CREATE TABLE `homework_account` (
  `id` int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `uuid` varchar(128) NOT NULL DEFAULT '',
  `password` varchar(128) NOT NULL DEFAULT '',
  `date_joined` timestamp NOT NULL default current_timestamp,
  `last_login` timestamp NOT NULL default '0000-00-00 00:00:00',
  `status` smallint UNSIGNED NOT NULL
) ENGINE=InnoDB default charset=utf8 comment="老师账号";

DROP TABLE IF EXISTS `homework_teacher`;
CREATE TABLE `homework_teacher`(
  `id` integer unsigned AUTO_INCREMENT NOT NULL,
  `account_id` int unsigned NOT NULL,
  `username` varchar(50) CHARACTER SET ucs2 NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='老师';

DROP TABLE IF EXISTS `homework_klass`;
CREATE TABLE `homework_klass`(
  `id` integer unsigned AUTO_INCREMENT NOT NULL,
  `name` varchar(50) CHARACTER SET ucs2 NOT NULL DEFAULT '',
  `teacher_id` int unsigned NOT NULL,
  `code` varchar(6) CHARACTER SET ucs2 NOT NULL,
  `book_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='班级';

DROP TABLE IF EXISTS `homework_klass_student`;
CREATE TABLE `homework_klass_student`(
  `id` integer unsigned AUTO_INCREMENT NOT NULL,
  `klass_id` int unsigned NOT NULL,
  `student_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_klass_id_student_id` (`klass_id`,`student_id`),
  index `uk_student_id_klass_id` (`student_id`,`klass_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='班级和学生多对多的中间表';

DROP TABLE IF EXISTS `homework_student`;
CREATE TABLE `homework_student`(
  `id` integer unsigned AUTO_INCREMENT NOT NULL,
  `account_id` int unsigned NOT NULL,
  `username` varchar(50) CHARACTER SET ucs2 NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学生';

DROP TABLE IF EXISTS `homework_book`;
CREATE TABLE `homework_book` (
  `id` integer unsigned AUTO_INCREMENT NOT NULL,
  `name` varchar(50) CHARACTER SET ucs2 NOT NULL DEFAULT '',
  `app_id` int unsigned NOT NULL,
  `cover` varchar(100) NOT NULL default '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='教材';

DROP TABLE IF EXISTS `homework_assignment`;
CREATE TABLE `homework_assignment` (
  `id` integer unsigned AUTO_INCREMENT NOT NULL,
  `klass_id` int unsigned NOT NULL,
  `book_id` int unsigned NOT NULL,
  `teacher_id` int unsigned NOT NULL,
  `publish_time` timestamp NOT NULL default current_timestamp,
  `finish_time` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作业';

DROP TABLE IF EXISTS `homework_assignment_stem`;
CREATE TABLE `homework_assignment_stem` (
  `id` integer unsigned AUTO_INCREMENT NOT NULL,
  `assignment_id` int unsigned NOT NULL,
  `unit` varchar(40) NOT NULL DEFAULT '',
  `lesson_name` varchar(40) NOT NULL DEFAULT '',
  `node_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作业的题目';

DROP TABLE IF EXISTS `homework_question_record`;
CREATE TABLE `homework_question_record` (
  `id` integer unsigned AUTO_INCREMENT NOT NULL PRIMARY KEY,
  `student_id` int unsigned NOT NULL,
  `assignment_id` int(10) unsigned DEFAULT NULL,
  `klass_id` int unsigned NOT NULL,
  `stem_id` int unsigned NOT NULL,
  `status` tinyint(2) unsigned NOT NULL,
  unique `uk_student_klass_question` (`student_id`, `assignment_id`, `klass_id`, `stem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作业做题记录';

DROP TABLE IF EXISTS `homework_student_assignment`;
CREATE TABLE `homework_student_assignment` (
  `id` integer unsigned AUTO_INCREMENT NOT NULL,
  `student_id` int unsigned NOT NULL,
  `assignment_id` int unsigned not NULL,
  `klass_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  unique `uk_student_klass_assignment` (`student_id`, `assignment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学生和作业的关联表';

/*!40101 SET character_set_client = @saved_cs_client */;
