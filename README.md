## pop 开发文档
本项目采用的是前后端分离的开发模式，使用json序列化的数据进行前后端沟通，其中，后端使用的是Django作为开发框架，前端使用的是vue作为前段工具。所有开发环境必须进行后端和前端的环境构建。
### 后端
#### 使用到的开发框架
Django 1.7.6
#### 代码目录
位于 `pop/homework`
#### 依赖的工具
 1. Python2.7
2. pip(Python的包管理软件)
3. virtualenv(Python的虚拟环境)
 4. Mysql
#### 环境构建
 * 安装上述的依赖工具。
 * 进入mysql，创建名为 pop 的 database。
* `cd pop`
* 编辑 pop/settings.py 修改`DATABASES`中的 `USER`, `PASSWORD`, `HOST`等值，让它和#1中保持一致
* `make venv`
* `source venv/bin/activate`
* `make deps` 安装依赖
* `make db` 
 * `python manage.py init_homework`进行数据初始化
* `make server` 启动后端服务器

### 前端
#### 使用到的开发框架是
vue 1.0.0 
#### 代码目录
位于 `pop/homework`
#### 依赖的工具
* node & npm
* webpack
* vue
#### 环境构建
* `cd pop`
* `make npm` 安装依赖
* `make wp-watch` 启动前端服务器监听代码变化
#### 发布前端代码
* 执行`make build`代码
* 编译后的代码存在于`front-src/bundles/`

### 使用办法
#### 学生测试地址
`http://127.0.0.1:8000/f/?username=%E6%9D%8E%E5%B0%8F%E8%8A%B1&password=21367EE73FB48E561E024E47DEA149D6&kind=2&user_id=11188fb44a50a3cf014a50a4b2c10000#!/index`
#### 老师测试地址
`http://127.0.0.1:8000/f/?username=%E5%91%A8%E8%80%81%E5%B8%88&password=21367EE73FB48E561E024E47DEA149D6&kind=1&user_id=8a488fb44a50a3cf014a50a4b2c10000#!/index`

#### 参数说明
username: 用户名称，来自于泡泡账户体系
user\_id: 账号，比如，8a488fb44a50a3cf014a50a4b2c10000
password: 密码，比如，21367EE73FB48E561E024E47DEA149D6
kind: 用户类型，1是老师，2是学生

#### 注意：
切换时候需要退出。
http://127.0.0.1:8000/logout
